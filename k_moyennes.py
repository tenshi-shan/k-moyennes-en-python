#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 20 16:54:03 2020

@author: C.R.
"""
import random
import copy
#import numpy

#Calcule le centre du groupe
def moyenne(groupe):
    taille=len(groupe[0])
    centre=[0 for x in range(taille)]
    nombre=0
    #centre=[x for x in range(taille)]
    for i in groupe:
        nombre+=1
        for j in range(taille):
            centre[j]+=i[j]
    centre=[x/nombre for x in centre]
    return centre
            
#Calcule la distance entre deux points
def distance(point, autre_point):
    dist=0
    for i in range(len(point)):
        #print(point[i], autre_point[i])
        dist+=(point[i] - autre_point[i])**2
    return dist**0.5

#Calcule la distance entre le point et le centre du groupe.
def distance_groupe(point, groupe):
    return distance(point, moyenne(groupe))

#Calcule le maximum de distnce entre des points et 
def max_distance(point, groupe):
    dist=0
    for i in groupe:
        dist=max(distance(point, i), dist)
    return dist

#Calcule l'index du maxmimum
def index_max_distance(point, groupe):
    index=-1
    index_max=0
    dist=0
    for i in groupe:
        index+=1
        if max(distance(point, i), dist) != dist:
            index_max=index
    return index_max

#Choisis le point parmi groupe le plus éloigné des points inclus dans groupes
def choix_point_opt(groupe, groupes):
    max_index=0
    max_somme=0
    for i in range(len(groupe)):
        somme=0
        for j in groupes:
            somme+=distance(groupe[i],j[0])
        if somme>max_somme:
            max_somme=somme
            max_index=i
    return max_index
            
#Choisis les nb_points points initiaux les plus optimaux dans groupe
#Retourne les points optimaux mais aussi groupe privé de ces points.
def choix_points_opt(groupe, nb_points):
    taille=len(groupe)
    groupes=[]
    tableau=copy.deepcopy(groupe)
    index=random.randrange(taille)
    groupes.append([tableau[index]])
    del tableau[index]
    for i in range(nb_points-1):
        index=choix_point_opt(tableau, groupes)
        groupes.append([tableau[index]])
        del tableau[index]
    return [groupes, tableau]

#Cherche à quel groupe attribuer le point selon les moyennes des groupes
def classer_point(point, groupes, moyennes):
    dist=0
    min_dist=float('inf')
    min_index=0
    for i in range(len(groupes)):
        dist=distance(point, moyennes[i])
        if dist<min_dist:
            min_index=i
            min_dist=dist
    groupes[min_index].append(point)
            
#Sert à classer les points de groupe dans groupes sans supprimer ce dernier dans groupe
def classer_groupe(groupe, groupes, moyennes):
    for i in groupe:
        classer_point(i, groupes, moyennes)
        
#Sert à classer les points de groupe tout en supprimant l'élément de groupe
def classer_groupe_sup(groupe, groupes, moyennes):
    for i in groupe:
        groupe.remove(i)
        classer_point(i, groupes, moyennes)

#Sert à reclasser les points dans groupes
def classer(groupes, moyennes):
    for vecteurs in groupes:
        classer_groupe_sup(vecteurs, groupes, moyennes)

#Sert à calculer les moyennes des groupes
def calcul_moyennes(groupes, moyennes):
    for i in range(len(groupes)):
        moyennes[i]=moyenne(groupes[i])

#Sert à trouver les classifications via l'algorithme de k-moyenne
def k_moyenne(groupe, nb_groupes, nb_iterations):
    [groupes, tableau]=choix_points_opt(groupe, nb_groupes)
    eph=[]
    moyennes=[x for x in range(len(groupes))]
    calcul_moyennes(groupes, moyennes)
    classer_groupe(tableau, groupes, moyennes)
    calcul_moyennes(groupes, moyennes)
    for i in range(nb_iterations-1):
        if eph!=groupes:
            classer(groupes, moyennes)
            calcul_moyennes(groupes, moyennes)
            eph=copy.deepcopy(groupes)
    return groupes